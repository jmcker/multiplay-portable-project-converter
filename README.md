# Multiplay Portable Project Converter #

Converts Multiplay projects that depend on local audio files into movable, self-contained projects. All necessary files are copied into a portable project folder, which can be moved to and used in a different folder or computer. 

v1.1

### Setup ###

1. Run converter.py
2. Select the XML show file you would like to convert
3. Locate the portable project folder and move it to its destination
4. Run the setup.py file located inside the portable folder
5. Select the XML show file named "*MyShow* - Portable Copy"
5. Open the show file in Multiplay

### System Requirements ###
* Python 2.x

### Contact ###

* Jack McKernan
* [jmcker@outlook.com](mailto:jmcker@outlook.com)